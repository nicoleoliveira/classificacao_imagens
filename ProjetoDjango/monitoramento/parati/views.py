from django.shortcuts import render
from django.template import loader
from parati.forms import DocumentForm
from parati.models import Imagem

def uploadImagem(request):
	form = DocumentForm()
	msg = 0

	if request.method == 'POST':
		form = DocumentForm(request.POST, request.FILES)
		if form.is_valid():
			imagem = str(form.cleaned_data["endereco"])
			print (imagem)
			tipo = imagem.split(".")
			print (tipo)
			if(tipo[1] in ["jpg", "JPG"]):
				form.save()
				msg = 1
			else:
				msg = 2
	return render(request, 'uploadImagem.html', {'form':form, 'msg':msg})

def index(request):
	return render(request, 'index.html')

def imagens(request):
	imgs = Imagem.objects.all();
	template = loader.get_template('imagens.html')
	return render(request, 'imagens.html', {'imagens':imgs})

def visualizarImagem(request, id):
	imagem = Imagem.objects.get(id=id)
	return render(request, 'visualizarImagem.html', {'imagem':imagem})
