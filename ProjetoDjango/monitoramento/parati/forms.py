from django import forms
from parati.models import Imagem

class DocumentForm(forms.ModelForm):
    class Meta:
    	model = Imagem
    	fields = '__all__'
