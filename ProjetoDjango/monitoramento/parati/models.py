from __future__ import unicode_literals

from django.db import models

class Imagem(models.Model):
	"""docstring for Imagem"""
	nome = models.CharField(max_length=35)
	endereco = models.FileField(upload_to='imagens/')
	descricao = models.TextField()

class ImagemComum(Imagem):
	pass

class ImagemClassificada(Imagem):
	"""docstring for ImagemClassificada"""
	data = models.DateField()
	imagem = models.ForeignKey('ImagemComum')

class TipoAmostra(models.Model):
	"""docstring for TipoAmostra"""
	nome = models.CharField(max_length=35)
	cor = models.CharField(max_length=35)

class Amostra(models.Model):
	"""docstring for Amostra"""
	xSuperior = models.DecimalField(max_digits=5, decimal_places=2)
	ySuperior = models.DecimalField(max_digits=5, decimal_places=2)
	xInferior = models.DecimalField(max_digits=5, decimal_places=2)
	yInferior = models.DecimalField(max_digits=5, decimal_places=2)
	tipoAmostra = models.ForeignKey('TipoAmostra')
	imagem = models.ForeignKey('ImagemComum')

class Entorno(models.Model):
	"""docstring for Entorno"""
	descricao = models.TextField()
